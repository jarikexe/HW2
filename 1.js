  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */


  //variables
  var button;

  //select all buttons
  button = document.getElementsByClassName("showButton"); //or q
  button[0].onclick = function() {
    alert("asdfasdf");
  }
  // coppy from https://www.w3schools.com/jsref/met_document_getelementsbyclassname.asp
  var i;
  for (i = 0; i < button.length; i++) {

      button[i].onclick = function(){
          select(this.dataset.tab);
      }
  }


  //select div thet should be visible
  function select(t){
    var visible;
    visible = document.querySelector("div[data-tab='"+t+"']");
    if(hideAllTabs()) visible.classList.add("active");
  }


  //hide all div
  function hideAllTabs(){
    document.querySelectorAll("div[data-tab]").forEach(function(elem) {
      elem.classList.remove("active");
    });
    return true;
  }
